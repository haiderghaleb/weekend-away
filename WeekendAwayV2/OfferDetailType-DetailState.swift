//
//  OfferDetailType-DetailState.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 17/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class OfferDetailType_DetailState: UICollectionViewFlowLayout {
    
    override func prepareLayout() {
//        super.prepareLayout()
        scrollDirection = .Horizontal
//        collectionView!.contentSize = CGSizeMake(320, 100)
        collectionView!.frame = CGRectMake(0, 0, 320, 150)
        itemSize = CGSizeMake(50, 50)
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
}
