//
//  APIcontroller.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import Foundation

@objc protocol APIcontrollerDelegate {
    optional func didLogin(result: AFOAuthCredential)
    optional func didGetCategoriesBoard(result: NSArray)
    optional func didGetCategoryList(result: NSDictionary)
}


let baseURL = NSURL(string: "http://54.77.64.208:8080")
let sessionManager : GROAuth2SessionManager = GROAuth2SessionManager(baseURL: baseURL, clientID: "clientapp_beta_0.3.0", secret: "ndsjkdnkjnkqjnd3nasnkdn")
let credentials = AFOAuthCredential.retrieveCredentialWithIdentifier(sessionManager.serviceProviderIdentifier)


class APIcontroller {
    
    var delegate : APIcontrollerDelegate?
    
    init(delegate: APIcontrollerDelegate?){
        self.delegate = delegate
    }
    
    func loginRequest(username: String, password: String){
        println(username)
        println(password)
        
        sessionManager.authenticateUsingOAuthWithPath("/oauth/token",
            username: username, // "devbeta"
            password: password, // "93jasldkj3ascn3aosinaksajsdasjkd2"
            scope: "read write",
            success: { (credential : AFOAuthCredential!) -> Void in
                println("I have a token! \(credential.accessToken)")
                AFOAuthCredential.storeCredential(credential, withIdentifier: sessionManager.serviceProviderIdentifier)
                if let apiDelegate = self.delegate? {
                    apiDelegate.didLogin!(credential)
                }
            },
            failure: { (error : NSError!) -> Void in
//                println("Error: \(error)")
                var credential : AFOAuthCredential = AFOAuthCredential()
                if let apiDelegate = self.delegate? {
                    apiDelegate.didLogin!(credential)
                }
            }
        )
    }
    
    func reOauth(refreshToken : String){
        sessionManager.authenticateUsingOAuthWithPath("/oauth/token",
            refreshToken: refreshToken,
            success: { (credential : AFOAuthCredential!) -> Void in
                println(credential)
                AFOAuthCredential.storeCredential(credential, withIdentifier: sessionManager.serviceProviderIdentifier)
            },
            failure: { (error : NSError!) -> Void in
                println("Error: \(error)")
            }
        )
    }
    
    func getCategoriesBoard(){
        let path = NSBundle.mainBundle().pathForResource("CategoriesBoard", ofType: "json")
        
        let jsonData = NSData(contentsOfFile: path!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: nil)
        
        let jsonResult = NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray

        if let apiDelegate = self.delegate? {
            apiDelegate.didGetCategoriesBoard!(jsonResult)
        }
        
//        sessionManager.setAuthorizationHeaderWithCredential(credentials)
//        
//        sessionManager.GET(
//            "/weaw/offerCategory",
//            parameters: nil,
//            success: { (sessionDataTask : NSURLSessionDataTask!, responseObject : AnyObject!) -> Void in
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoriesBoard!(responseObject as NSArray)
//                }
//            },
//            failure: { (sessionDataTask : NSURLSessionDataTask!, error: NSError!) -> Void in
//                println("Error: " + error.localizedDescription)
//                println("Error: \(error)")
//                var emptyDic = []
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoriesBoard!(emptyDic)
//                }
//            }
//        )
        
    }
    
    func getCategoryList(categoryType: String){
        let path = NSBundle.mainBundle().pathForResource("CategoryList", ofType: "json")
        
        let jsonData = NSData(contentsOfFile: path!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: nil)
        
        let jsonResult = NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
        
        if let apiDelegate = self.delegate? {
            apiDelegate.didGetCategoryList!(jsonResult)
        }
        
        
        
//        sessionManager.setAuthorizationHeaderWithCredential(credentials)
//        
//        sessionManager.GET(
//            "/weaw/offerCategory/\(categoryType)",
//            parameters: nil,
//            success: { (sessionDataTask : NSURLSessionDataTask!, responseObject : AnyObject!) -> Void in
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoryList!(responseObject as NSDictionary)
//                }
//            },
//            failure: { (sessionDataTask : NSURLSessionDataTask!, error: NSError!) -> Void in
//                println("Error: " + error.localizedDescription)
//                println("Error: \(error)")
//                var emptyDic = Dictionary<String, String>()
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoryList!(emptyDic)
//                }
//            }
//        )
    }
    
//    func getCategoryList(searchTerm: String) {
//        let manager = AFHTTPRequestOperationManager()
//        
//        manager.GET("http://pixabay.com/api/?username=haiderghaleb&key=a0395ff9d2cba3d0a068&search_term=\(searchTerm)&image_type=photo",
//            parameters: nil,
//            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
//                println("JSON Passed")
//                println("http://pixabay.com/api/?username=haiderghaleb&key=a0395ff9d2cba3d0a068&search_term=\(searchTerm)&image_type=photo")
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoryList!(responseObject as NSDictionary)
//                }
//
//            },
//            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
//                println("Error: " + error.localizedDescription)
//                var emptyDic = Dictionary<String, String>()
//                if let apiDelegate = self.delegate? {
//                    apiDelegate.didGetCategoryList!(emptyDic)
//                }
//            }
//        )
//    }
}

