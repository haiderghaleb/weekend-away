//
//  CategoryListDetailModelStruct.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import Foundation
import UIKit

class categoryListDetailModelStruct {
     var previewURL : String = ""
    
    class func loadJSON(result: NSDictionary) -> categoryListDetailModelStruct {
        var cat = categoryListDetailModelStruct()
//        var hitResults = result["hits"] as NSArray
//        for hitResult in hitResults {
            cat.previewURL = result["previewURL"]! as String
//        }
        return cat
    }
}