//
//  UIElementStyling.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/10/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

    
func UITextFieldPadding(view: UITextField, image: String, paddingX: CGFloat, paddingY: CGFloat, paddingLeft: CGFloat, paddingTop: CGFloat){
    view.leftViewMode = UITextFieldViewMode.Always
    view.leftView = UIView(frame: CGRectMake(paddingX, paddingY, paddingLeft, paddingTop))
}

func UITextFieldWithIcon(view: UITextField, image: String, padding: CGFloat, placeholder: UInt?, fontName: String?, fontSize: CGFloat?){
    view.leftViewMode = UITextFieldViewMode.Always
    var imageView = UIImageView()
    imageView.contentMode = UIViewContentMode.Center
    imageView.image = UIImage(named: image)
    imageView.layer.frame = CGRectMake(0, 0, padding, 0)
    view.leftView = imageView
    if ((fontName != nil) && (fontSize != nil)) {
        view.font = UIFont(name: fontName!, size: fontSize!)
    }
    if (placeholder != nil) {
        placeholderColor(view, UIColorFromRGB(placeholder!, 1.0))
    }
}

func GaussianBlur(ImageBlurSize: CGSize, imageToBlur: UIImage, imageRadiusSize: Float) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(ImageBlurSize, true, 0.0)
    var imgaa :UIImage = UIGraphicsGetImageFromCurrentImageContext();
    var ciimage :CIImage = CIImage(image: imageToBlur)
    var filter : CIFilter = CIFilter(name:"CIGaussianBlur")
    
    filter.setDefaults()
    filter.setValue(ciimage, forKey: kCIInputImageKey)
    filter.setValue(imageRadiusSize, forKey: kCIInputRadiusKey)
    
    var outputImage : CIImage = filter.outputImage
    var context : CIContext = CIContext(options: nil)
    var rect : CGRect = outputImage.extent()
    
    rect.origin.x += (rect.size.width - imageToBlur.size.width) / 2
    rect.origin.y += (rect.size.height - imageToBlur.size.height) / 2
    rect.size = imageToBlur.size
    
    var cgimg : CGImageRef = context.createCGImage(outputImage, fromRect: rect)
    var finalImage : UIImage = UIImage(CGImage: cgimg)!
    UIGraphicsEndImageContext();
    
    return finalImage
}

func placeholderColor(textField: UITextField, color: UIColor?) {
    var newColor : UIColor!
    if color != nil {
        newColor = color
    } else {
        newColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    }
    let attrs = [NSForegroundColorAttributeName: newColor]
    let placeHolderStr = NSAttributedString(string: textField.placeholder!, attributes: attrs)
    textField.attributedPlaceholder = placeHolderStr
}

func UIColorFromRGB(rgbValue: UInt, alpha: CGFloat) -> UIColor {
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(alpha)
    )
}

func UIColorFromRGBUsingHexString(colorCode: String, alpha: CGFloat) -> UIColor {
    
    var rgbValue : UInt32 = 0
    var scan : NSScanner = NSScanner(string: "0x\(colorCode)")
    scan.scanHexInt(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(alpha)
    )
}

func customFont(name: String, size: CGFloat?) -> UIFont {
    return UIFont(name: "Helvetica Neue Light", size: 15.0)!
}

func imageFromURL(url: String) -> UIImage{
    let url = NSURL(string: url)
    var err: NSError?
    var imageData = NSData(contentsOfURL: url!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
    
    return UIImage(data: imageData!)!
}

func customNavigationBar(navigationBar: UINavigationBar, backgroundColor: UIColor, itemColor: UIColor) {
    navigationBar.barTintColor = backgroundColor
    navigationBar.tintColor = itemColor
    navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:itemColor]
}

func removeBottomborder(navigationBar: UINavigationBar) {
    for parent in navigationBar.subviews {
        for childView in parent.subviews {
            if(childView is UIImageView) {
                childView.removeFromSuperview()
            }
        }
    }
}

func attributeString(ressource: String, string: String) -> NSMutableAttributedString{
    var attributedRessource = NSMutableAttributedString(string: ressource)
    
    let priceLen = string as NSString
    let countStr = countElements(ressource)
    let aRange = NSMakeRange(countStr - priceLen.length, priceLen.length)
    
    let theFont = UIFont(name: "Helvetica Neue Bold", size: 11.0)
    let theColor = UIColor.whiteColor()
    
    // Textfarbe
//    attributedRessource.addAttribute(NSForegroundColorAttributeName, value: theColor, range: aRange)
    
    // Hintergrundfarbe
//    attributedRessource.addAttribute(NSBackgroundColorAttributeName, value: UIColor.redColor(), range: aRange)
    
    // Schriftart
    attributedRessource.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica Bold", size: 15.0)!, range: aRange)
    
    return attributedRessource
}






