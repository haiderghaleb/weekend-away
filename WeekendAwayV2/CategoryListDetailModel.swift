//
//  CategoryListDetailModel.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import Foundation

class CategoryListDetailModel {
    
    var cityPrice : Int!
    var cityName : String!
    var imageURLs : [String]!
    var offerID : String!
    
    init(cityName: String, cityPrice: Int, imageURLs: [String], offerID: String){
        self.cityName = cityName
        self.cityPrice = cityPrice
        self.imageURLs = imageURLs
        self.offerID = offerID
    }
    
    class func getJSON(result: NSArray) -> [CategoryListDetailModel] {
        var catResutls = [CategoryListDetailModel]()
        var imagesArr = [String]()
        
        if result.count > 0 {
            for res in result {
                var cityName = res["cityName"] as String
                var cityPrice = res["price"] as Int
                
                var imageURLs = res["pictures"] as NSArray
                imagesArr.removeAll(keepCapacity: true)
                for imageURL in imageURLs {
                    var imageURL = imageURL["url"] as String
                    imagesArr.append(imageURL)
                }
                
                var offerID = res["offerParent"] as String
                
                var catResutl = CategoryListDetailModel(cityName: cityName, cityPrice: cityPrice, imageURLs: imagesArr, offerID: offerID)
                catResutls.append(catResutl)
            }
        }
        return catResutls
    }

}