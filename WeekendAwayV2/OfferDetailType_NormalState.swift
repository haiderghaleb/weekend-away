//
//  OfferDetailType_NormalState.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 17/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class OfferDetailType_NormalState: UICollectionViewFlowLayout {
   
    override func prepareLayout() {
//        super.prepareLayout()
        scrollDirection = .Vertical
//        collectionView!.contentSize = CGSizeMake(320, 300)
        collectionView!.frame = CGRectMake(0, 300, 320, 200)
        itemSize = CGSizeMake(100, 100)
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
    
}
