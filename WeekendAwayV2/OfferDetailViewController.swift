//
//  OfferDetailViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 09/11/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit



class OfferDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var offerID = ""
    var imagesArray : [String] = []
    var layoutChanged : Bool! = false
    
    @IBOutlet weak var cityImageCarousel: iCarousel!
    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var cityImageIndicator: UIPageControl!
    @IBOutlet weak var offerDetailTypeControllerView: UIView!
    @IBOutlet weak var offerDetailView: UIView!
    @IBOutlet weak var offerDetailSelectorsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        cityImageCarousel.delegate = self
//        cityImageCarousel.dataSource = self
//        cityImageCarousel.type = .Linear
//        cityImageCarousel.pagingEnabled = true
//        
//        cityImageCarousel.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var selectionArray : [String] = ["Flight","Hotel","City","Events","Activities","Weather"]
        
//        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("typeSelectorCell", forIndexPath: indexPath) as OfferDetailTypeCollectionViewCell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("typeSelectorCell", forIndexPath: indexPath) as UICollectionViewCell
        
//        cell.typeImageView.backgroundColor = UIColor.grayColor()
//        cell.typeImageView.layer.cornerRadius = cell.typeImageView.frame.size.width / 2
//        cell.typeImageView.clipsToBounds = true
//        
//        cell.typeName.text = selectionArray[indexPath.row]
//        
//        layoutChanged = false
        
        cell.backgroundColor = UIColor.redColor()
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("selected index: \(indexPath.row)")
        
        if layoutChanged == false {
            layoutChanged = true
            spring(0.7, { () -> Void in
                collectionView.setCollectionViewLayout(OfferDetailType_DetailState(), animated: false)
                collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .CenteredHorizontally)
                collectionView.invalidateIntrinsicContentSize()
            })
        }
        else {
            layoutChanged = false

            
            spring(0.7, { () -> Void in

                collectionView.setCollectionViewLayout(OfferDetailType_NormalState(), animated: false)
                collectionView.invalidateIntrinsicContentSize()
            })
        }

//        collectionView.performBatchUpdates({ () -> Void in
//            }, completion: { (completed : Bool) -> Void in
//                collectionView.reloadData()
//        })
//        collectionView.reloadData()
//        collectionView.setCollectionViewLayout(OfferDetailType_DetailState(), animated: true)
//        collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .CenteredHorizontally)
//        if layoutChanged != true {
//            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//            layout.itemSize = CGSize(width: 100, height: 100)
//            collectionView.showsHorizontalScrollIndicator = false
//
//            layout.scrollDirection = .Horizontal
//            layout.invalidateLayout()
//            spring(0.7, { () -> Void in
//                collectionView.setCollectionViewLayout(layout, animated: true)
//                println(self.offerDetailView.constraints())
//                self.offerDetailSelectorsCollectionView.frame = CGRectMake(0, 0, 320, 100)
//                self.offerDetailSelectorsCollectionView.layer.needsDisplayOnBoundsChange = true
//                self.offerDetailSelectorsCollectionView.sizeToFit()
//                self.layoutChanged = true
//            })
//
//        } else if layoutChanged == true {
//            collectionView.setCollectionViewLayout(offerDetailSelectorsCollectionView.collectionViewLayout, animated: true)
//        } else {
//            println("test")
//        }
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel!) -> Int {
        return imagesArray.count
    }
    
//    func carousel(carousel: iCarousel!, viewForItemAtIndex index: Int,var reusingView view: UIView!) -> UIView! {
//    
//        var imageURL = NSURL(string: imagesArray[index])
//        var imageUrlRequest = NSURLRequest(URL: imageURL!)
//        if (view == nil) {
//            view = UIImageView(frame: self.cityImageView.frame)
//            (view as UIImageView!).contentMode = self.cityImageView.contentMode
//            (view as UIImageView!).clipsToBounds = self.cityImageView.clipsToBounds
//            (view as UIImageView!).setImageWithURL(imageURL)
//            (view as UIImageView!).tag = index
//        } else {
//            //get a reference to the label in the recycled view
//            self.cityImageView = view.viewWithTag(index) as UIImageView!
//        }
//        
//        return view
//    }
//    
//    func carousel(carousel: iCarousel!, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
//        if (option == .Wrap) {
//            return 1
//        }
//        return value
//    }
//    
//    func carouselCurrentItemIndexDidChange(carousel: iCarousel!) {
//        cityImageIndicator.currentPage = carousel.currentItemIndex
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
