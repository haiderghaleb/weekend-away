//
//  CategoryCustomTableViewCell.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 05/10/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryDescription: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
