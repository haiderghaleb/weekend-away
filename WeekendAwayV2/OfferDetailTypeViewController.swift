//
//  OfferDetailTypeViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 09/11/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class OfferDetailTypeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var offerDetailTypeViewController: UICollectionView!
    
    var sizeChanged : Bool! = false
    var index : Int! = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        println("OfferDetailTypeViewController loaded")
        println(self.offerDetailTypeViewController)
        offerDetailTypeViewController.collectionViewLayout.invalidateLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        if sizeChanged == true {
            var svc = parentViewController as OfferDetailViewController
//            svc.offerDetailTypeControllerView.frame = CGRectMake(0, 100, self.view.frame.width, 100)
//            svc.offerDetailTypeControllerView.bounds = CGRectMake(0, 100, self.view.frame.width, 350)
//            svc.offerDetailTypeControllerView.layer.frame = CGRectMake(0, 100, self.view.frame.width, 350)
//            svc.offerDetailTypeControllerView.layer.bounds = CGRectMake(0, 100, self.view.frame.width, 350)
            svc.offerDetailTypeControllerView.backgroundColor = UIColor.blackColor()
            
//            offerDetailTypeViewController.frame = CGRectMake(0, 100, self.view.frame.width, 350)
//            offerDetailTypeViewController.bounds = CGRectMake(0, 100, self.view.frame.width, 350)
            
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: 100, height: 100)
            layout.scrollDirection = .Horizontal
            layout.invalidateLayout()

//            self.offerDetailTypeViewController.setCollectionViewLayout(layout, animated: false)
//            offerDetailTypeViewController.invalidateIntrinsicContentSize()
//            offerDetailTypeViewController.layoutIfNeeded()
            //            offerDetailTypeViewController.updateConstraintsIfNeeded()

            
            offerDetailTypeViewController.collectionViewLayout.shouldInvalidateLayoutForBoundsChange(CGRectMake(0, 0, 320, 100))

            self.offerDetailTypeViewController.updateConstraints()
            self.offerDetailTypeViewController.invalidateIntrinsicContentSize()
            self.offerDetailTypeViewController.updateConstraintsIfNeeded()
            self.offerDetailTypeViewController.setCollectionViewLayout(layout, animated: false)

        }

//        if sizeChanged == true {
//            self.offerDetailTypeViewController.bounds = CGRectMake(0, 0, 320, 120)
//            self.view.frame.size = CGSizeMake(320, 140)
//            
//            springWithDelay(0.7, 0.04) {
//                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//                layout.itemSize = CGSize(width: 100, height: 100)
//                layout.scrollDirection = .Horizontal
//                
//                self.offerDetailTypeViewController.setCollectionViewLayout(layout, animated: false)
//            }
//        } else if sizeChanged == false {
//            
//        } else {
//            
//        }
//        
//        if sizeChanged == true && index != index {
//            println("sizeChanged == \(sizeChanged) && \(index) != \(index)")
//            self.offerDetailTypeViewController.bounds = CGRectMake(0, 0, 320, 120)
//            self.view.frame.size = CGSizeMake(320, 140)
//            
//            springWithDelay(0.7, 0.04) {
//                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//                layout.itemSize = CGSize(width: 100, height: 100)
//                layout.scrollDirection = .Horizontal
//                
//                self.offerDetailTypeViewController.setCollectionViewLayout(layout, animated: false)
//            }
//        } else if sizeChanged != true && index == index {
//            println("else")
//            println("sizeChanged == \(sizeChanged) && \(index) == \(index)")
//            self.offerDetailTypeViewController.bounds = self.offerDetailTypeViewController.bounds
//            self.view.frame.size = CGSizeMake(320, 220)
//            springWithDelay(0.7, 0.04) {
//            }
//        } else {
//            
//        }
    }
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        var size : CGSize!
//        if collectionView.collectionViewLayout.shouldInvalidateLayoutForBoundsChange(<#newBounds: CGRect#>) == "" {
//            size = CGSizeMake(100, 100)
//        } else {
//            size = CGSizeMake(50, 50)
//        }
//        return size
//    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("offerDetailType", forIndexPath: indexPath) as OfferDetailTypeCollectionViewCell
        
        cell.typeImageView.backgroundColor = UIColor.grayColor()
        cell.typeImageView.layer.cornerRadius = cell.typeImageView.frame.size.width / 2
        cell.typeImageView.clipsToBounds = true
        
        cell.typeName.text = "Test \(indexPath.row)"

        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("selected index: \(indexPath.row)")
        println("index: \(index)")
        
        var svc = parentViewController as OfferDetailViewController
//        svc.offerDetailTypeControllerView.frame = CGRectMake(0, 60, 320, 400)
//        svc.didPress(collectionView)
        if sizeChanged != true {
            sizeChanged = true
            viewWillLayoutSubviews()
        }
        
//        if sizeChanged != true && index == -1 {
//            println("sizeChanged == \(sizeChanged) && \(index) == \(index)")
//            index = indexPath.row
//            svc.didPress(collectionView)
//            
//        } else if index == indexPath.row && sizeChanged == true {
//            println("sizeChanged == \(sizeChanged) && \(index) == \(index)")
//            println("already clicked on me")
//            svc.didPress(collectionView)
//        } else {
//            println("sizeChanged == \(sizeChanged) && \(index) != \(index)")
//            index = indexPath.row
//            collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .CenteredHorizontally)
//            println("content should be changed & selection")
//        }
        
        
//        var cell = collectionView.cellForItemAtIndexPath(indexPath) as OfferDetailTypeCollectionViewCell
//        let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("offerDetailType", forIndexPath: indexPath) as OfferDetailTypeCollectionViewCell
//        cell.typeImageView.frame = CGRectMake(0, 0, 40, 40)
//        cell.typeImageView.backgroundColor = UIColor.grayColor()
//        cell.typeImageView.layer.cornerRadius = cell.typeImageView.frame.size.width / 2
//        cell.typeImageView.clipsToBounds = true
    }
    
//    func animateView() {
//        
//        let scale = CGAffineTransformMakeScale(offerDetailTypeViewController.layer.frame.origin.x, offerDetailTypeViewController.layer.frame.origin.y)
//        let scale1 = CGAffineTransformMakeScale(0.3, 0.3)
//        let translate = CGAffineTransformMakeTranslation(0, 0)
//        
//        offerDetailTypeViewController.transform = CGAffineTransformConcat(scale, translate)
//        offerDetailTypeViewController.alpha = 0
//        
//        spring(0.7) {
//            let scale = CGAffineTransformMakeScale(1, 1)
//            let translate = CGAffineTransformMakeTranslation(0, -150)
//            self.offerDetailTypeViewController.transform = CGAffineTransformConcat(scale, translate)
//            self.offerDetailTypeViewController.alpha = 1
//        }
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
