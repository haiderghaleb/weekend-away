//
//  CategoryListDataViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryListDataViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, APIcontrollerDelegate, CategoryListTableViewCellDelegate {

    @IBOutlet weak var categoryDetailsTable: UITableView!
    var offers = [CategoryListDetailModel]()
    lazy var apiController : APIcontroller = APIcontroller(delegate: self)
    var searchTerm = ""
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //        categoryDetailsTable = nil
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityIndicator.frame = CGRectMake(100, 100, 100, 100);
        activityIndicator.startAnimating()
        self.view.addSubview( activityIndicator )
        
        self.categoryDetailsTable.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func load(string: String) {
        self.apiController.getCategoryList(string)
    }
    
    func loadAPI(search: String) {
        self.apiController.getCategoryList(search)
    }

    func didPress(view: UIView!) {
        performSegueWithIdentifier("offerDetailSegue", sender: view)
    }
    
    func didGetCategoryList(result: NSDictionary){
        var clientOfferSummaries : NSArray = result["clientOfferSummaries"] as NSArray
        dispatch_async(dispatch_get_main_queue(), {
            self.offers = CategoryListDetailModel.getJSON(clientOfferSummaries)
            springWithCompletion(0.3, { () -> Void in
                
                let scale = CGAffineTransformMakeScale(0, 0)
                let translate = CGAffineTransformMakeTranslation(0, -150)
                
                self.categoryDetailsTable.alpha = 0
                self.activityIndicator.startAnimating()
                self.categoryDetailsTable.dataSource = self
                self.categoryDetailsTable.delegate = self
                
                }, { (complete : Bool) -> Void in
                    
                    let scale = CGAffineTransformMakeScale(1, 1)
                    let translate = CGAffineTransformMakeTranslation(0, 0)
                    
                    self.categoryDetailsTable.transform = CGAffineTransformConcat(scale, translate)
                    self.categoryDetailsTable.alpha = 1
                    self.activityIndicator.stopAnimating()
                    self.categoryDetailsTable.reloadData()
            })
        })
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("CategoryListCell", forIndexPath: indexPath) as CategoryListTableViewCell
        
        var offer = offers[indexPath.row]
        
        cell.cityName.text = offer.cityName
        cell.cityPrice.text = "€\(String(offer.cityPrice))"
        cell.loadImagesCarousel(offer.imageURLs)
        cell.delegate = self
        
        return cell
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "offerDetailSegue" {
            let offerDetailView = segue.destinationViewController as OfferDetailViewController
            if sender?.isKindOfClass(CategoryListTableViewCell) == true {
                let indexPath : NSIndexPath = categoryDetailsTable.indexPathForCell(sender as CategoryListTableViewCell)!
                var offer = offers[indexPath.row]
                
                offerDetailView.title = offer.cityName
                offerDetailView.offerID = offer.offerID
                offerDetailView.imagesArray = offer.imageURLs
            }
        }
    }
    

}
