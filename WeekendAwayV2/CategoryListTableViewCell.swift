//
//  CategoryListTableViewCell.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 04/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

@objc protocol CategoryListTableViewCellDelegate {
    optional func didPress(view: UIView!)
}

class CategoryListTableViewCell: UITableViewCell, iCarouselDataSource, iCarouselDelegate {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var cityName : UILabel!
    @IBOutlet weak var cityPrice: UILabel!
    @IBOutlet weak var imageCarousel: iCarousel!
    
    var imagesArray : [String] = []
    var delegate : CategoryListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cityName.text = cityName.text
        cityPrice.text = cityPrice.text
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func image(url: String) {
        
        var imageURL = NSURL(string: url)
        var img = NSURLRequest(URL: imageURL!)
        categoryImageView.backgroundColor = UIColor.grayColor()
        categoryImageView.image = nil
        categoryImageView.alpha = 0
        categoryImageView.setImageWithURLRequest(img, placeholderImage: UIImage(named: "backpacker"), success: { (request : NSURLRequest!, response : NSHTTPURLResponse!, image : UIImage!) -> Void in
                self.categoryImageView.image = image
                self.categoryImageView.alpha = 1
            }) { (request : NSURLRequest!, response : NSHTTPURLResponse!, err : NSError!) -> Void in
            println("Error: \(err)")
        }
    }
    
    // iCarousel configuration

    func loadImagesCarousel(images: [String]){
        // iCarousel
        imagesArray = images
        imageCarousel.delegate = self
        imageCarousel.dataSource = self
        imageCarousel.type = .Linear
        imageCarousel.pagingEnabled = true
        
        imageCarousel.reloadData()
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel!) -> Int {
        return imagesArray.count
    }
    
//    func numberOfPlaceholdersInCarousel(carousel: iCarousel!) -> Int {
//        return 0
//    }
//    
    func carousel(carousel: iCarousel!, viewForItemAtIndex index: Int,var reusingView view: UIView!) -> UIView! {
        var imageURL = NSURL(string: imagesArray[index])
        var imageUrlRequest = NSURLRequest(URL: imageURL!)
        if (view == nil) {
            view = UIImageView(frame: self.imageCarousel.frame)
            
            (view as UIImageView!).setImageWithURL(imageURL)
            (view as UIImageView!).tag = index
        } else {
            //get a reference to the label in the recycled view
            self.categoryImageView = view.viewWithTag(index) as UIImageView!
        }
        
        
        return view
    }
    
    
    func carousel(carousel: iCarousel!, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .Spacing) {
            return value * 1.1
        }
        if (option == .Wrap)
        {
            return 1
        }
        return value
    }

    func carousel(carousel: iCarousel!, didSelectItemAtIndex index: Int) {
        delegate?.didPress!(self)
    }
}
