//
//  ViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 03/10/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class ViewController: UIViewController, APIcontrollerDelegate {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var appBrandingView: UIView!
    @IBOutlet weak var loginFormView: UIView!
    @IBOutlet weak var loginWithSocialMediaView: UIView!
    
    lazy var apiController : APIcontroller = APIcontroller(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.backgroundImageView.image = GaussianBlur(view.bounds.size, backgroundImageView.image!, 2.5)
        UITextFieldWithIcon(usernameTextField, "Shape-2", 45, 0x434a54, "Helvetica Neue Light", 15.0)
        UITextFieldWithIcon(passwordTextField, "Lock", 45, 0x434a54, "Helvetica Neue Light", 15.0)
        
        animateView()
        

    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }
    
    @IBAction func unwindToLoginView(segue: UIStoryboardSegue){
        println("i'm pressed unwindtologinview")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func didLogin(result: AFOAuthCredential) {
        if result.isKindOfClass(AFOAuthCredential) && result.accessToken != nil {
            println(result.accessToken)
            performSegueWithIdentifier("categoriesListSegue", sender: self)
        } else {
            println("Username or password isn't correct")
            
            if usernameTextField.text == "" {
                placeholderColor(usernameTextField, UIColor.redColor())
                self.usernameTextField.textColor = UIColor.blackColor()
            } else if usernameTextField.text != "" {
                self.usernameTextField.textColor = UIColor.redColor()
            } else {
                self.usernameTextField.textColor = UIColor.blackColor()
            }
            if passwordTextField.text == "" {
                placeholderColor(passwordTextField, UIColor.redColor())
                self.passwordTextField.textColor = UIColor.blackColor()
            } else if passwordTextField.text != "" {
                self.passwordTextField.textColor = UIColor.redColor()
            } else {
                self.passwordTextField.textColor = UIColor.blackColor()
            }
        }
    }
    
    @IBAction func loginBtn(sender: AnyObject) {
        println("Login Pressed")
        self.apiController.loginRequest(self.usernameTextField.text, password: self.passwordTextField.text)
    }
    
    func animateView() {
        let scale = CGAffineTransformMakeScale(0.3, 0.3)
        let translate = CGAffineTransformMakeTranslation(0, -150)
        
        appBrandingView.transform = CGAffineTransformConcat(scale, translate)
        appBrandingView.alpha = 0
        
        loginFormView.transform = CGAffineTransformConcat(scale, translate)
        loginFormView.alpha = 0
        
        loginWithSocialMediaView.transform = CGAffineTransformConcat(scale, translate)
        loginWithSocialMediaView.alpha = 0
        
        spring(0.7) {
            let scale = CGAffineTransformMakeScale(1, 1)
            let translate = CGAffineTransformMakeTranslation(0, 0)
            self.appBrandingView.transform = CGAffineTransformConcat(scale, translate)
            self.appBrandingView.alpha = 1
        }
        
        springWithDelay(0.7, 0.05) {
            let scale = CGAffineTransformMakeScale(1, 1)
            let translate = CGAffineTransformMakeTranslation(0, 0)
            self.loginFormView.transform = CGAffineTransformConcat(scale, translate)
            self.loginFormView.alpha = 1
        }
        springWithDelay(0.7, 0.10, {
            let scale = CGAffineTransformMakeScale(1, 1)
            let translate = CGAffineTransformMakeTranslation(0, 0)
            self.loginWithSocialMediaView.transform = CGAffineTransformConcat(scale, translate)
            self.loginWithSocialMediaView.alpha = 1
        })
    }
    
}

