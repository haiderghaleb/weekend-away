//
//  OfferDetailTypeCollectionViewCell.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 09/11/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class OfferDetailTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var typeName: UILabel!
    @IBOutlet weak var typeImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
