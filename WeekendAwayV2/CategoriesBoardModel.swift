//
//  CategoriesBoardModel.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 07/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import Foundation

class CategoriesBoardModel {
    var name : String!
    var price : Int!
    var picture : String!
    var color : String!
    
    init(name: String, price: Int, picture: String, color: String){
        self.name = name
        self.price = price
        self.picture = picture
        self.color = color
    }
    
    class func getJSON(result: NSArray) -> [CategoriesBoardModel] {
        var catResutls = [CategoriesBoardModel]()
        if result.count > 0 {
            for res in result {
//                println(res)
                var name = res["name"] as String
                var price = res["price"] as Int
                var picture = res["pictures"] as String
                var color = res["color"] as String
                var catResutl = CategoriesBoardModel(name: name, price: price, picture: picture, color: color)
                println(catResutl)
                catResutls.append(catResutl)
            }
        }
        
        return catResutls
    }
}