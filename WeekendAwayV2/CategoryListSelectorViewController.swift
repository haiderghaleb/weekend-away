//
//  CategoryListSelectorViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 02/11/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryListSelectorViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var categorySelectionCollectionView: UICollectionView!
//    var tableData = ["Backpacker","Economy","Luxury","Adventure","Medical"]
    var tableData : [String] = []
//    var selectedItemIndex : Int! = 3
    var selectedItemIndex : Int!
    var url = ""
    var autoLoad : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        categorySelectionCollectionView.backgroundColor = UIColorFromRGB(0x60b9dd, 1.0)
        categorySelectionCollectionView.tintColor = UIColor.whiteColor()
//        categorySelectionCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        categorySelectionCollectionView.contentOffset = CGPointMake(20, categorySelectionCollectionView.contentOffset.y)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadTableView(index: Int) {
        var svc : CategoryListDataViewController = parentViewController?.childViewControllers[1] as CategoryListDataViewController
        svc.searchTerm = tableData[index]
        svc.loadAPI(url)
//        svc.load(tableData[index])
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as CategoryListSelectorCollectionViewCell
        
        if indexPath.row == selectedItemIndex && autoLoad == true {
            collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .CenteredHorizontally)
            autoLoad = false
            selectedCell(cell)
            loadTableView(indexPath.row)
        } else if cell.selected == true {
            selectedCell(cell)
        } else if cell.selected == false {
            deselectCell(cell)
        } else {
            println("error: \(cell)")
        }
        
        cell.categoryName.text = tableData[indexPath.row]
        return cell
    }
    
    // Select & Deselect
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("didSelectItemAtIndexPath cell: \(indexPath.row)")
        collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .CenteredHorizontally)
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as CategoryListSelectorCollectionViewCell
        selectedCell(cell)
        loadTableView(indexPath.row)
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        println("didDeselectItemAtIndexPath cell: \(indexPath.row)")
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? CategoryListSelectorCollectionViewCell
        deselectCell(cell)
    }
    
    func selectedCell(cell: CategoryListSelectorCollectionViewCell) {
        cell.selected = true
        cell.backgroundColor = UIColorFromRGB(0x000000, 0.25)
        cell.layer.cornerRadius = 10
        cell.categoryName.textColor = UIColor.whiteColor()
    }
    
    func deselectCell(cell: CategoryListSelectorCollectionViewCell?) {
        cell?.selected = false
        cell?.backgroundColor = UIColor.clearColor()
        cell?.categoryName.textColor = UIColorFromRGB(0xFFFFFF, 0.5)
    }
    
    
    // Cell layout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var label = UILabel(frame: CGRectZero)
        label.text = tableData[indexPath.row]
        var maximumLabelSize : CGSize = CGSizeMake(130, 35)
        var expectedSize : CGSize = label.sizeThatFits(maximumLabelSize)
        label.frame = CGRectMake(10, 0, expectedSize.width+20, 35)
        return label.frame.size
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
