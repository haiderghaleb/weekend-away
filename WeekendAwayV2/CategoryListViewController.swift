//
//  CategoryListViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 12/10/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryListViewController: UIViewController {
    
    var items: [String] = []
    var index : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBottomborder(navigationController!.navigationBar)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "categorySelectorItemsSegue" {
            let categorySelectorViewController = segue.destinationViewController as CategoryListSelectorViewController
            categorySelectorViewController.tableData = items
            var type = ""
            if index == 0 {
                type = "lowBudget"
            } else if index == 1 {
                type = "midBudget"
            } else if index == 2 {
                type = "highBudget"
            } else {
                println(index)
            }
            
            categorySelectorViewController.selectedItemIndex = index
            categorySelectorViewController.url = type
        }
    }

}
