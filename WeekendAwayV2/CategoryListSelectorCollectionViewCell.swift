//
//  CategoryListSelectorCollectionViewCell.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 02/11/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryListSelectorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryName: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        categoryName.sizeToFit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
