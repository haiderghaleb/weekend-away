//
//  OfferDetailType-NormalState.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 17/11/14.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import Cocoa

class OfferDetailType_NormalState: UICollectionViewFlowLayout {

    override func prepareLayout() {
//        headerReferenceSize = CGSizeMake(320, 50)
//        footerReferenceSize = CGSizeMake(320, 50)
        scrollDirection = .Vertical
        collectionView!.frame = CGRectMake(0, 300, 320, 200)
    }
    
    
}
