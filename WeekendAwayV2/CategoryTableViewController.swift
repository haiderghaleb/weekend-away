//
//  CategoryTableViewController.swift
//  WeekendAwayV2
//
//  Created by Haider Ghaleb on 05/10/2014.
//  Copyright (c) 2014 The D. GmbH. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController, APIcontrollerDelegate {

    var catBoard : [String] = []
    var categoriesBoard = [CategoriesBoardModel]()
    var selectedIndex : Int!
    lazy var apiController : APIcontroller = APIcontroller(delegate: self)
    var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var categoriesBoardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Hide & disable back button
        navigationItem.setHidesBackButton(true, animated: true)
        
        // Custom navigationBar
        
        let navBar = navigationController?.navigationBar
        customNavigationBar(navBar!, UIColorFromRGB(0x60b9dd, 1.0), UIColor.whiteColor())
        
        let rootViewController: ViewController = storyboard?.instantiateViewControllerWithIdentifier("ViewController") as ViewController
        navigationController?.viewControllers.insert(rootViewController, atIndex: 0)
        
        self.apiController.getCategoriesBoard()
        
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityIndicator.frame = CGRectMake(100, 100, 100, 100);
        activityIndicator.startAnimating()
        self.view.addSubview( activityIndicator )
        
    }
    
    
    func didGetCategoriesBoard(result: NSArray) {
        println("Category Board result: \(result.count)")
        
        dispatch_async(dispatch_get_main_queue(), {
            self.categoriesBoard = CategoriesBoardModel.getJSON(result)
            self.activityIndicator.stopAnimating()
            self.categoriesBoardTableView.reloadData()
        })
        
        println(categoriesBoard)
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = false
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return categoriesBoard.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath) as CategoryCustomTableViewCell
        
        // Configure the cell...
        
        var categoiesBoardList = categoriesBoard[indexPath.row]
        catBoard.append(categoiesBoardList.name)

        let pictures = ["backpacker","midpacker","luxury", "backpacker"]
        let prices = ["€200","€350","€500","€200"]
        let colors : [UInt] = [0x00b5d1,0xc1cb75,0xf7c05b,0x00b5d1]
        let images = ["backpacker.jpg","midpacker.jpg","luxury.jpg"]
        
        // Name & Price
        var string = "\(categoiesBoardList.name) Weekend Starts from €\(categoiesBoardList.price)"
        cell.categoryDescription.attributedText = attributeString(string, String(categoiesBoardList.price))
        
        // Image
//        cell.categoryImageView.image = imageFromURL(categoiesBoardList.picture)
        cell.categoryImageView.image = UIImage(named: images[indexPath.row])
        // BackgroundColor
        cell.categoryView.backgroundColor = UIColorFromRGBUsingHexString(categoiesBoardList.color, 0.8)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("TableView selected \(indexPath.row)")
        selectedIndex = indexPath.row
    }

    @IBAction func logout(sender: AnyObject) {
        println("logout btn clicked")
        
        
        navigationController?.popToRootViewControllerAnimated(true)
//        navigationController?.popToViewController(root, animated: true)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "categorySelectionSegue" {
            let categorySelectorViewController = segue.destinationViewController as CategoryListViewController
            
            categorySelectorViewController.items = catBoard
            println("tableView.indexPathForSelectedRow(): \(tableView.indexPathForSelectedRow()!.row)")
            
            categorySelectorViewController.index = tableView.indexPathForSelectedRow()!.row
        }
        
    }


}
